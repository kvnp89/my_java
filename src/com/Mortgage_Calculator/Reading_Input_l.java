package com.Mortgage_Calculator;

import java.util.Scanner;

public class Reading_Input_l {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);// reading input from terminal
        //System.out.println("Age: ");// println method calls anew line so we use print method
        System.out.print("Age: ");
        byte age =  scanner.nextByte();
        System.out.println("you are " +age );
        System.out.print("Name:");
        String name = scanner.nextLine().trim();
        System.out.println("Your are "+ name);
    }
}
