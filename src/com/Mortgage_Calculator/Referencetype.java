package com.Mortgage_Calculator;

import java.awt.*;
import java.util.Date;

public class Referencetype {
    public static void main(String[] args) {
        byte age = 30;// primitive type we dont need to allocate memory java wil take care
        Date now = new Date();
        // reference type we need to allocate memory using operator () main diff primitive and reference
        // now is the object for class Date() this objects have members
        System.out.println(now);
        // memeory managment
        // primetive types have independent memory as shown below
        byte x = 1;
        byte y = x;
        System.out.println(x);
        System.out.println(y);
        x = 2;
        System.out.println(x);
        System.out.println(y);
        System.out.println("------ reference types");
        Point point1 = new Point(1,2);
        Point point2 = point1;
        // reference type stores addres of memory to variable ponit 1 and 2. they store reference of data
        System.out.println(point1);
        System.out.println(point2);
        point1.x=2;
        System.out.println(point1);
        System.out.println(point2);
    }
}
