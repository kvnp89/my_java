package com.Mortgage_Calculator;

public class If_Statments {
    public static void main(String[] args) {
        int temparature = 32;
        if (temparature > 30) {
            System.out.println("It's  hot day");
            System.out.println("drink water");
        }else if (temparature > 20 && temparature <= 30){
            System.out.println("Beautiful Day");
        }else System.out.println("Cold day");
        ///
        System.out.println("simplifying if statements");

        int income = 120000;
        boolean hasHighIncome = income > 100000;
        System.out.println(hasHighIncome);
        // ternary operator
        System.out.println("Ternary operator");
        String className = income >100000 ? "First" : "Economy";

        //
    }
}
