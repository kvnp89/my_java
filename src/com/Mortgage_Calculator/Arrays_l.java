package com.Mortgage_Calculator;

import java.util.Arrays;

public class Arrays_l {
    public static void main(String[] args) {
        // arrays are refrence types
        int[] numbers = new int[5];
        numbers[0] = 1;
        numbers[1] = 2;
        System.out.println(numbers); // print array java print address of memory
        System.out.println(Arrays.toString(numbers));
        // new way to create arrays if we know items early
        int[] numbers1 = {1,2,3,4,5,6};
        System.out.println(numbers1.length);
        // arrays are fixed lengh we cant add new
        Arrays.sort(numbers1);
        System.out.println(Arrays.toString(numbers1));

        // multi dimension arrays
        System.out.println("======= Multi dimensional array");
        int[][] mul_numbers = new int[2][3];
        mul_numbers[0][0] = 1; // acess index of 1st row and 1st column
        System.out.println(Arrays.deepToString(mul_numbers));
        System.out.println(mul_numbers);
        // new way
        int[][] mul_numbers1 = { {1,2,3},{4,5,6}};
        System.out.println(Arrays.deepToString(mul_numbers1));

    }
}
