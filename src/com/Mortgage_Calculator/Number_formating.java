package com.Mortgage_Calculator;

import java.text.NumberFormat;

public class Number_formating {
    public static void main(String[] args) {
       // NumberFormat currency = new NumberFormat() //. ist an abstract cant be initiated
        NumberFormat currency =  NumberFormat.getCurrencyInstance(); // factory method
        String result = currency.format(1234567.891);
        System.out.println(result);
        NumberFormat percent = NumberFormat.getPercentInstance();
        String result2 = percent.format(0.325);
        System.out.println(result2);
        String result3 = NumberFormat.getPercentInstance().format(0.25);
        System.out.println(result3);
    }
}
