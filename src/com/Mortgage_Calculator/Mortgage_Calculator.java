package com.Mortgage_Calculator;

import java.text.NumberFormat;
import java.util.Scanner;

public class Mortgage_Calculator {
    public static void main(String[] args) {
        System.out.println("Mortgage Calculator");
        Scanner scanner = new Scanner(System.in);
        int principle = 0;
        float ar = 0;
        float r = 0;
        int yn = 0;
        int n = 0;
        while (true){
            System.out.println("Enter Amount Between 1,000 and 1,00,000.");
            System.out.print("Principal Amount: ");
            principle = scanner.nextInt();
            if (principle >= 1000 && principle <= 100000){
                break;
            }
        }
        while(true){
            System.out.print("Annual Interest Rate: ");
            ar = scanner.nextFloat();
            if (ar > 0 && ar <= 10 ){
                r = (ar/100)/12;
                break;
            }
            System.out.println("Enter a value grater than 0 and less than or equal to 30");
        }
        while (true){
            System.out.println("Enter a value between 1 and 30");
            System.out.print("Period(Years): ");
            yn = scanner.nextInt();
            if (yn > 1 && yn <= 30){
                n = yn * 12;
                break;
            }
        }

        System.out.println(" For Principal Amount:" + principle + " Annual interest:" + ar + " Period(years):"+yn);

        double x = r*(Math.pow(1+r,n));
        double y = (Math.pow(1+r,n))-1;
        double m = principle*(x/y);
        System.out.println("Mortgage: "+ NumberFormat.getCurrencyInstance().format(m));


    }
}
