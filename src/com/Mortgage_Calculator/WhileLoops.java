package com.Mortgage_Calculator;

import java.util.Locale;
import java.util.Scanner;

public class WhileLoops {
    public static void main(String[] args) {
        int i = 5;
        while (i >3){
            System.out.println("hellow world "+ i);
            i --;
        }
        Scanner scanner = new Scanner(System.in);
        String input = "";
        while (!input.equals("quit")){
            System.out.println("input: ");
            input= scanner.next().toLowerCase();
            System.out.println(input);

        }

    }
}
