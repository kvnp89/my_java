package com.Mortgage_Calculator;

public class casting_l {
    public static void main(String[] args) {
        // implicit casting example short automatically converted to int
        // byte> short > int > long > float > double
        short x = 1;
        int y = x+2;
        System.out.println(y);
        double a = 1.1;
        double b = a+2;
        // explicit casting  this can happen only compatable not sting and int
        int c = (int)a+2;
        System.out.println(b);
        System.out.println(c);
        // rapper class to cast string and int
        String p = "1";
        int q = Integer.parseInt(p) +2;
        System.out.println(q);
    }

}
