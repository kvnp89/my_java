package com.Mortgage_Calculator;

public class Strings_learn {
    public static void main(String[] args) {

        String message = "  Hello Pavan + !!  ";

        System.out.println(message);
        System.out.println(message.endsWith("!!"));
        System.out.println(message.length());
        System.out.println("index of o is " + message.indexOf("o"));
        System.out.println("replace fucn = "+ message.replace("!", "@"));
        System.out.println(message.toLowerCase());
        System.out.println(message.trim());
        // escape sequence include special chars in string
        String message1 = "Hello\"Pavan\""; // main reason why we need to use forward slash in java not like windows
        String message2 = "c:\tWindows\\...";
        String message3 = "c:\nWindows\\...";
        System.out.println(message1);
        System.out.println(message2);
        System.out.println(message3);
    }
}
