package com.Mortgage_Calculator;

public class ForeachLoop {
    public static void main(String[] args) {
        String[] fruits = {"Apple", "Orange", "Mango"};
        System.out.println(fruits.length);

        for (int i = 0; i < fruits.length; ++i){
            System.out.println(fruits[i] + i);
        }
        // for each loop
        for (String fruit : fruits)
            System.out.println(fruit);

    }
}
